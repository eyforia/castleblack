package com.b2s.castleblack.service

import spock.lang.Specification

public class AdminSpec extends Specification {

    void "admin user must have access to the admin page"() {

        given: "page1 is accessible only by admin users"
        def permissions = [page1: ["sysAdmin", "varAdmin"], page2: ["user"]]
        def service = new AuthorizationService(permissions: permissions)

        and: "user has both admin roles"
        def user = new User(name: "user1", roles: ["sysAdmin", "varAdmin"])

        expect: "user can access admin page"
        user.roles
        user.active
        service.hasAccess(user, "page1")
    }

    void "all users must have default role"() {
        when:
        def user = new User(name: "user1")

        then: "user must have at least 1 role"
//            Assert.assertTrue( user.roles.size()>0) //junit style
//            assert user.roles.size()>0  //groovy assert
        user.roles.size() > 0  //we're in "then" block - ever line is an assertion
    }

}
