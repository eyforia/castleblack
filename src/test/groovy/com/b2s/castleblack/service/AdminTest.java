package com.b2s.castleblack.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class AdminTest {

    @Test
    public void testAdminUserHasAccessToAdminPage() {
        Map<String, List<String>> permissions = new HashMap<>();
        permissions.put("page1", Arrays.asList("sysAdmin", "varAdmin"));
        permissions.put("page2", Collections.singletonList("user"));

        AuthorizationService service = new AuthorizationService();
        service.setPermissions(permissions);

        User user = new User();
        user.setName("user1");
        user.setRoles(Arrays.asList("sysAdmin", "varAdmin"));

        boolean authorized = service.hasAccess(user, "page1");


        Assert.assertTrue("User must have access to the page", authorized);
    }
}
