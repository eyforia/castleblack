package com.b2s.castleblack.service

import com.b2s.castleblack.dao.PermissionDAO
import com.b2s.castleblack.model.Permission
import spock.lang.Specification
import spock.lang.Subject

class PermissionsServiceSpec extends Specification {

    AuditService auditService = Mock(AuditService.class)
    PermissionDAO dao = Mock(PermissionDAO)

    @Subject
    PermissionsService service = new PermissionsService(
            auditService:auditService,
            permissionDAO: dao
    )


    def "hasPermission - happy path"() {
        given:
        def user = new User(id: 123)
        def permissionName = "test-permission"

        when: "service is called to check if a user has a permission"
        def result = service.hasPermission(user, permissionName)

        then: "it must call DAO, and DAO should returns a list of 1 permission"
        1 * dao.findForUser(123) >> [new Permission(name: permissionName)]

        then: "it must NOT call update on the DAO"
        0 * dao.add(*_)

        then:
        result
    }

    def "addPermission - happy path"() {
        given:
            def permissionName = "test-permission"
            def user = new User(id: 123)

        when:
            def result = service.addPermission(user, permissionName)

        then: "dao is called to add a record"
            1 * dao.add(user.id, _) >> { int userId, List<Permission> permissions ->
                assert userId==user.id
                assert permissions.size()==1
                assert permissions[0].name == permissionName
                assert permissions[0].userId == user.id
                return true
            }

        then:
            result
    }
}
