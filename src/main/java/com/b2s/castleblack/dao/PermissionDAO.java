package com.b2s.castleblack.dao;

import com.b2s.castleblack.model.Permission;

import java.util.List;

public interface PermissionDAO {

    List<Permission> findForUser(int userId);

    void add(int userId, List<Permission> permissions);

}
