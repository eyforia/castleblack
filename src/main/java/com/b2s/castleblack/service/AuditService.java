package com.b2s.castleblack.service;

import com.b2s.castleblack.model.Permission;

public interface AuditService {

    public void onPermissionAdded(int userId, Permission permission);
}
