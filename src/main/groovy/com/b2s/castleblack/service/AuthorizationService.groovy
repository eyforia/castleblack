package com.b2s.castleblack.service;

public class AuthorizationService {
    Map<String, List<String>> permissions = [:]

    public boolean hasAccess(User user, String page) {
        return permissions[page]?.intersect(user.roles)
    }
}
