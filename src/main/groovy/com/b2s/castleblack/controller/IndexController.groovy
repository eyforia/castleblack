package com.b2s.castleblack.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class IndexController {

    @RequestMapping("/index.json")
    @ResponseBody
    def get() {
        return [name: 'test', phone: '111-222-3333']
    }

}
